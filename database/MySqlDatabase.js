import mysql from "mysql"

export class MySqlDatabase {

    constructor() {
        this._db = this.createConnection();
    }

    /**
     * @param databaseQuery
     * @param params
     * @return {Promise}
     */
    async query(databaseQuery, params = []) {
        return new Promise(((resolve, reject) => {
            this._db.query(databaseQuery, params, (err, rows) => {
                if (err) {
                    reject(err);
                }

                resolve(rows);
            })
        }));
    }

    createConnection() {
        console.log("Creating MySQL Connection . . .")

        const config = {
            database: process.env.DB_NAME,
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD
        }

        return mysql.createConnection(config);
    }
}
