import express from "express";
import dotenv from "dotenv";
import morgan from "morgan";
import bodyParser from "body-parser";

dotenv.config();

import { MySqlDatabase } from "./database/MySqlDatabase.js";

const db = new MySqlDatabase();

const app = express();
const port = process.env.PORT;

app.use(morgan("dev"))
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get("/", async (req, res) => {

    const result = await db.query("SELECT * FROM users");

    res.status(200).send(result);
})

app.listen(port, () => console.log(`Listening on port ${port} . . .`));
